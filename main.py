import sys


class Variable:
    def __init__(self, name, value):
        self.name = name
        self.value = value


def readVariable(instruction):
    varName = instruction.split(",")[1]
    value = input("Zadajte hodnotu premennej " + varName + " : ")

    while (isNumber(value) == False):
        value = input(
            "Premenna " + varName + " musi obsahovat cele cislo(kladne alebo zaporne). Zadajte znova hodnotu premennej " + varName + " : ")

    newVar = Variable(varName, value)
    return addVarToVariables(newVar)


def addVarToVariables(newVar):
    already_presented = False

    for var in variables:
        if (var.name == newVar.name):
            var.value = newVar.value
            already_presented = True
            break

    if (already_presented == False):
        if (startsWithLetter(newVar.name)):
            variables.append(newVar)
        else:
            print(
                "Chyba na riadku " + str(
                    position) + "! Premenna " + newVar.name + " sa nezacina velkym ani malym pismenom, a teda nemoze byt definovana")
            global cancel_execution
            cancel_execution = True


def writeVariable(instruction):
    inputVar = instruction.split(",")[1]

    value = getValueOf(inputVar)

    if (value != "not_present"):
        print("Obsah premennej " + inputVar + " : " + str(value))
    else:
        print("Chyba na riadku " + str(position) + "! Premenna " + inputVar + " nebola definovana")
        global cancel_execution
        cancel_execution = True


def jump(instruction):
    jump_to = 0
    jump_type = instruction.split(",")[0]
    if (jump_type == "JUMP"):
        if(isNumber(instruction.split(",")[1])):
            jump_to = int(instruction.split(",")[1])
        else:
            print("Chyba na riadku " + str(position) + "! Hodnota " + instruction.split(",")[
                1] + " nie je celociselna konstanta")
            global cancel_execution
            cancel_execution = True
    elif (jump_type == "JUMPT" or jump_type == "JUMPF"):
        inputVar = getValueOf(instruction.split(",")[1])
        if (inputVar != "not_present"):
            canJump = False

            if ((jump_type == "JUMPT" and inputVar != 0) or (jump_type == "JUMPF" and inputVar == 0)):
                canJump = True

            if (canJump):
                jump_to = getValueOf(instruction.split(",")[2])
                if (jump_to == "not_present"):
                    jump_to = "not_defined"

        else:
            jump_to = "not_defined"

    return jump_to


def getValueOf(name):
    if (isNumber(name)):
        return int(name)
    else:
        for var in variables:
            if (var.name == name):
                if (var.value == "FALSE"):
                    return 0
                elif (var.value == "TRUE"):
                    return 1

                return int(var.value)

    return "not_present"


def compare(instruction):
    operation_type = instruction.split(",")[0]
    value = getValueOf(instruction.split(",")[1])
    value2 = getValueOf(instruction.split(",")[2])
    new_var_name = instruction.split(",")[3]

    if (value != "not_present"):
        value = int(value)
        if (operation_type == ">"):
            if (value > value2):
                addVarToVariables(Variable(new_var_name, "TRUE"))
            else:
                addVarToVariables(Variable(new_var_name, "FALSE"))
        elif (operation_type == "<"):
            if (value < value2):
                addVarToVariables(Variable(new_var_name, "TRUE"))
            else:
                addVarToVariables(Variable(new_var_name, "FALSE"))
        elif (operation_type == ">="):
            if (value >= value2):
                addVarToVariables(Variable(new_var_name, "TRUE"))
            else:
                addVarToVariables(Variable(new_var_name, "FALSE"))
        elif (operation_type == "<="):
            if (value <= value2):
                addVarToVariables(Variable(new_var_name, "TRUE"))
            else:
                addVarToVariables(Variable(new_var_name, "FALSE"))
        elif (operation_type == "=="):
            if (value == value2):
                addVarToVariables(Variable(new_var_name, "TRUE"))
            else:
                addVarToVariables(Variable(new_var_name, "FALSE"))
    else:
        print("Chyba na riadku " + str(position) + "! Premenna " + instruction.split(",")[
            1] + " nie je definovana a zaroven to nie je celociselna konstanta")
        global cancel_execution
        cancel_execution = True


def isNumber(inputString):
    result = all(char.isdigit() for char in inputString)
    if (result == False):
        if (inputString[0] == "-" and all(char.isdigit() for char in inputString[1:])):
            result = True

    return result


def startsWithLetter(variable):
    ascii_letters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    return ascii_letters.__contains__(variable[0]);


def count(instruction):
    global cancel_execution
    operation_type = instruction.split(",")[0]

    value = getValueOf(instruction.split(",")[1])
    value2 = getValueOf(instruction.split(",")[2])

    if (value != "not_present" and value2 != "not_present"):
        if (operation_type == "+"):
            addVarToVariables(Variable(instruction.split(",")[3], value + value2))
        elif (operation_type == "-"):
            addVarToVariables(Variable(instruction.split(",")[3], value - value2))
        elif (operation_type == "*"):
            addVarToVariables(Variable(instruction.split(",")[3], value * value2))
    elif (value == "not_present"):
        print("Chyba na riadku " + str(position) + "! Premenna s nazvom " + instruction.split(",")[
            1] + " nebola definovana")
        cancel_execution = True
    elif (value2 == "not_present"):
        print("Chyba na riadku " + str(position) + "! Premenna s nazvom " + instruction.split(",")[
            2] + " nebola definovana")
        cancel_execution = True


def executeOperation(instruction):
    operation_type = instruction.split(",")[0]
    if (operation_type == "="):
        value = getValueOf(instruction.split(",")[2])
        if (value != "not_present"):
            newVar = Variable(instruction.split(",")[1], value)
            addVarToVariables(newVar)
        else:
            print("Chyba na riadku " + str(position) + "! Premenna s nazvom " + instruction.split(",")[
                2] + " nebola definovana, a zaroven nie je celym cislom")
            global cancel_execution
            cancel_execution = True

    elif (operation_type.__contains__(">") or operation_type.__contains__("<") or operation_type == "=="):
        compare(instruction)
    else:
        count(instruction)


file_path = "instructions.txt"

if (len(sys.argv) > 1):
    file_path = sys.argv[1]

with open(file_path) as f:
    lines = f.readlines()

lines = [x.strip() for x in lines]
print(lines)
variables = []
position = 1
cancel_execution = False

while (position <= len(lines)):
    instruction = lines[position - 1]

    if (instruction.__contains__("NOP")):
        position += 1

    elif (instruction.__contains__("READ")):
        result = readVariable(instruction)
        position += 1

    elif (instruction.__contains__("WRITE")):
        writeVariable(instruction)
        position += 1

    elif (instruction.__contains__("JUMP")):
        jump_to = jump(instruction)

        if (jump_to == "not_defined"):
            print("Chyba na riadku " + str(position) + "! Premenna s nazvom " + instruction.split(",")[
                1] + " nebola definovana")
            break
        elif (jump_to != 0):
            if (jump_to > len(lines)):
                print("Chyba na riadku " + str(position) + "! Skok na neexistujuci riadok " + str(
                    jump_to) + " (subor ma " + str(len(
                    lines)) + " riadkov)")
                break
            else:
                position = jump_to
        else:
            position += 1
    else:
        executeOperation(instruction)
        position += 1

    if (cancel_execution):
        break
